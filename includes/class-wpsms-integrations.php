<?php

namespace WP_SMS;

use WP_SMS\Gateway\loginpanel;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

class Integrations {

	public $sms;
	public $date;
	public $options;
	public $cf7_data;

	public function __construct() {
		global $sms;

		$this->sms     = $sms;
		$this->date    = WP_SMS_CURRENT_DATE;
		$this->options = Option::getOptions();

		// Contact Form 7
		if ( isset( $this->options['cf7_metabox'] ) ) {
			add_filter( 'wpcf7_editor_panels', array( $this, 'cf7_editor_panels' ) );
			add_action( 'wpcf7_after_save', array( $this, 'wpcf7_save_form' ) );
			add_action( 'wpcf7_before_send_mail', array( $this, 'wpcf7_sms_handler' ) );
		}

		// Woocommerce
		if ( isset( $this->options['wc_notif_new_order'] ) ) {
			add_action( 'woocommerce_new_order', array( $this, 'wc_new_order' ) );
		}

		// EDD
		if ( isset( $this->options['edd_notif_new_order'] ) ) {
			add_action( 'edd_complete_purchase', array( $this, 'edd_new_order' ) );
		}
		if(isset( $this->options['send_new_user_password'])){
				add_action('takexpert_send_new_user_password_to_phone', array($this, 'reset_and_send_user_new_password'), 10, 2);
		}
	}

	public function cf7_editor_panels( $panels ) {
		$new_page = array(
			'wpsms' => array(
				'title'    => __( 'SMS Notification', 'wp-sms' ),
				'callback' => array( $this, 'cf7_setup_form' )
			)
		);

		$panels = array_merge( $panels, $new_page );

		return $panels;
	}

	public function cf7_setup_form( $form ) {
		$cf7_options       = get_option( 'wpcf7_sms_' . $form->id() );
		$cf7_options_field = get_option( 'wpcf7_sms_form' . $form->id() );

		if ( ! isset( $cf7_options['phone'] ) ) {
			$cf7_options['phone'] = '';
		}
		if ( ! isset( $cf7_options['message'] ) ) {
			$cf7_options['message'] = '';
		}
		if ( ! isset( $cf7_options_field['phone'] ) ) {
			$cf7_options_field['phone'] = '';
		}
		if ( ! isset( $cf7_options_field['message'] ) ) {
			$cf7_options_field['message'] = '';
		}

		include_once WP_SMS_DIR . "includes/templates/wpcf7-form.php";
	}

	public function wpcf7_save_form( $form ) {
		update_option( 'wpcf7_sms_' . $form->id(), $_POST['wpcf7-sms'] );
		update_option( 'wpcf7_sms_form' . $form->id(), $_POST['wpcf7-sms-form'] );
	}

	public function wpcf7_sms_handler( $form ) {
		$cf7_options       = get_option( 'wpcf7_sms_' . $form->id() );
		$cf7_options_field = get_option( 'wpcf7_sms_form' . $form->id() );
		$this->set_cf7_data();

		if ( $cf7_options['message'] && $cf7_options['phone'] ) {
			$this->sms->to = explode( ',', $cf7_options['phone'] );

			$this->sms->msg = preg_replace_callback( '/%([a-zA-Z0-9._-]+)%/', function ( $matches ) {
				foreach ( $matches as $item ) {
					if ( isset( $this->cf7_data[ $item ] ) ) {
						return $this->cf7_data[ $item ];
					}
				}
			}, $cf7_options['message'] );

			$this->sms->SendSMS();
		}

		if ( $cf7_options_field['message'] && $cf7_options_field['phone'] ) {
		    $to = preg_replace_callback( '/%([a-zA-Z0-9._-]+)%/', function ( $matches ) {
		        foreach ( $matches as $item ) {
					if ( isset( $this->cf7_data[ $item ] ) ) {
						return $this->cf7_data[ $item ];
					}
				}
			}, $cf7_options_field['phone'] );

            // Check the type of field is select.
            foreach ($form->scan_form_tags() as $scan_form_tag) {
                if($scan_form_tag['basetype'] == 'select') {
                    foreach ($scan_form_tag['raw_values'] as $raw_value) {
                        $option = explode('|', $raw_value);

                        if(isset($option[0]) and $option[0] == $to) {
                            $to = $option[1];
                        }
                    }
                }
            }

            $this->sms->to = array( $to );

			$this->sms->msg = preg_replace_callback( '/%([a-zA-Z0-9._-]+)%/', function ( $matches ) {
			    foreach ( $matches as $item ) {
					if ( isset( $this->cf7_data[ $item ] ) ) {
						return $this->cf7_data[ $item ];
					}
				}
			}, $cf7_options_field['message'] );
			$this->sms->SendSMS();
		}
	}

	private function set_cf7_data() {
		foreach ( $_POST as $index => $key ) {
			if ( is_array( $key ) ) {
				$this->cf7_data[ $index ] = implode( ', ', $key );
			} else {
				$this->cf7_data[ $index ] = $key;
			}
		}
	}

	public function wc_new_order( $order_id ) {
		$order          = new \WC_Order( $order_id );
		$this->sms->to  = explode(',', $this->options['admin_mobile_number']);
		$template_vars  = array(
			'%order_id%'     => $order_id,
			'%status%'       => $order->get_status(),
			'%order_number%' => $order->get_order_number(),
		);
		$message        = str_replace( array_keys( $template_vars ), array_values( $template_vars ), $this->options['wc_notif_new_order_template'] );
		$this->sms->msg = $message;

		$this->sms->SendSMS();
	}

	public function edd_new_order( $payment_id ) {

            $payment_meta = edd_get_payment_meta( $payment_id );
            $cart_items_list = "";
            $has_advisor_request = FALSE;
            $has_another_orders = FALSE;
						$total_price = 0;
            foreach($payment_meta['cart_details'] as $cart_item){
                if($cart_item['id'] == $this->options['advisor_product_id']){
                    $has_advisor_request = TRUE;
                }
                else {
                    $has_another_orders = TRUE;
                    $cart_items_list .= '- '. edd_get_cart_item_name($cart_item).' ('.$cart_item['quantity'].'عدد)'.PHP_EOL;
                }
								$total_price += $cart_item['price'];
            }

            $user = get_userdata( $payment_meta['user_info']['id'] );

            $template_vars = array(
                '%edd_email%'    => $user->user_email,
                '%edd_first%' => $user->user_firstname,
                '%edd_last%' => $user->user_lastname,
                '%edd_items%' => $cart_items_list,
                '%edd_payment_id%' => $payment_id,
								'%edd_total_price%' => $total_price,
            );

						if(isset($this->options['customer_phone_field'])){
							$customer_phone_fields = explode('|', str_replace(' ', '', $this->options['customer_phone_field']));
							foreach ($customer_phone_fields as $customer_phone_field) {
								if(isset($user->$customer_phone_field)){
									$user_phone_no_validation = $user->$customer_phone_field;
								} else {
									$user_phone_no_validation = get_user_meta($user_id, $customer_phone_field, true);
								}
								if(preg_match('/^09[0-9]{9}$/', $user_phone_no_validation)){
										$template_vars['%edd_phone%'] = $user_phone = $user_phone_no_validation;
										break;
								}
							}
						}

            if($has_another_orders){
                // Send SMS to admin for product orders
                $this->sms->to  = explode(',', $this->options['admin_mobile_number']);
                $message        = str_replace( array_keys( $template_vars ), array_values( $template_vars ), $this->options['edd_notif_new_order_template'] );
                if(!empty($message)){
                    $this->sms->msg = $message;
                    $this->sms->SendSMS();
                }
                // Send SMS to customer for product orders
                if ( isset( $user_phone ) ) {
                    $this->sms->to  = array( $user_phone );
                    $message        = str_replace( array_keys( $template_vars ), array_values( $template_vars ), $this->options['edd_notif_customer_new_order_template'] );
                    if(!empty($message)){
                        $this->sms->msg = $message;
                        $this->sms->SendSMS();
                    }
                }
            }

            // check order has advisor product
            if($has_advisor_request){
                // Send SMS to admin for advisor
                $this->sms->to  = explode(',', $this->options['admin_mobile_number']);
                $message        = str_replace( array_keys( $template_vars ), array_values( $template_vars ), $this->options['edd_notif_new_order_advisor_template'] );
                if(!empty($message)){
                    $this->sms->msg = $message;
                    $this->sms->SendSMS();
                }
                // Send SMS to customer for advisor
                if ( isset( $user_phone ) ) {
                    $this->sms->to  = array( $user_phone );
                    $message        = str_replace( array_keys( $template_vars ), array_values( $template_vars ), $this->options['edd_notif_customer_new_order_advisor_template'] );
                    if(!empty($message)){
                        $this->sms->msg = $message;
                        $this->sms->SendSMS();
                    }
                }
            }
	}

	public function reset_and_send_user_new_password( $user_id, $new_password ) {
			$user_phone = get_userdata( $user_id )->user_login;
			if(preg_match('/^09[0-9]{9}$/', $user_phone)){
				$this->sms->to = array( $user_phone );
				$pattern_code = preg_split('/^pattern(\s)*:(\s)*/i', $this->options['send_new_user_password_template'])[1];
				if(!empty($pattern_code) && $this->options['gateway_name'] == 'mdpanel'){
							if ( ! class_exists( 'nusoap_client' ) ) {
								include_once WP_SMS_DIR . 'includes/libraries/nusoap.class.php';
							}
							$client = new \nusoap_client("http://188.0.240.110/class/sms/wsdlservice/server.php?wsdl");
							$client->soap_defencoding = 'UTF-8';
							$client->decode_utf8      = true;
							$user = $this->options['gateway_username'];
							$pass = $this->options['gateway_password'];
							$fromNum = $this->options['gateway_sender_id'];
							$toNum = array($user_phone);
							$pattern_code = $pattern_code;
							$input_data = array("OTP" => $new_password);
							$client->call("sendPatternSms", array($fromNum, $toNum, $user, $pass, $pattern_code, $input_data));
						}
				}
		}
}
new Integrations();
